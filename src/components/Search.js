import React, { useState, useEffect } from 'react'
import axios from 'axios'

const Search = () => {
  const [term, setTerm] = useState('programming')
  const [debouncedTerm, setDebouncedTerm] = useState(term)
  const [results, setResults] = useState([])

  const handleTermChange = e => setTerm(e.target.value)

  useEffect(() => {
    const timerId = setTimeout(() => setDebouncedTerm(term), 1000)

    return () => clearTimeout(timerId)
  }, [term])

  useEffect(() => {
    const search = async () => {
      const {
        data: {
          query: { search: searchResults },
        },
      } = await axios.get('https://en.wikipedia.org/w/api.php', {
        params: {
          action: 'query',
          list: 'search',
          origin: '*',
          format: 'json',
          srsearch: debouncedTerm,
        },
      })

      setResults(searchResults)
    }

    search()
  }, [debouncedTerm])

  const renderedResults = results.map(({ title, snippet, pageid }) => (
    <div className='item' key={pageid}>
      <div className='right floated content'>
        <a
          className='ui button'
          href={`https://en.wikipedia.org?cuird=${pageid}`}
          target='_blank'
          rel='noreferrer'
        >
          Go
        </a>
      </div>
      <div className='content'>
        <div className='header'>{title}</div>
        <span dangerouslySetInnerHTML={{ __html: snippet }} />
      </div>
    </div>
  ))

  return (
    <div>
      <div className='ui form'>
        <div className='field'>
          <label>Enter serach term</label>
          <input className='input' onChange={handleTermChange} value={term} />
        </div>
      </div>
      <div className='ui celled list'>{renderedResults}</div>
    </div>
  )
}

export default Search
