import React from 'react'
import PropTypes from 'prop-types'

const Link = ({href, children, className}) => {
  const onClick = e => {
    if(e.metaKey || e.ctrlKey){
      return
    }

    e.preventDefault()
    window.history.pushState({}, '', href)

    const navEvent = new PopStateEvent('popstate')
    window.dispatchEvent(navEvent)
  }

  return <a href={href} className={className} onClick={onClick}>{children}</a>
}

Link.propTypes = {
  href: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
}

export default Link
