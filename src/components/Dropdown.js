import React, {useState, useEffect, useRef} from 'react'
import PropTypes from 'prop-types'

const Dropdown = ({options, selected, setSelected, label}) => {
  const [open, setOpen] = useState(false)
  const divRef = useRef()

  const renderedOptions = options.map((option, index) => {
    const {label} = option
    if(selected.value === option.value) return null

    return (
      <div key={index} className='item' onClick={() => setSelected(option)}>
        {label}
      </div>
    )
  })

  useEffect(() => {
    const onBodyClick = e => {
      if(divRef.current.contains(e.target)) return
      setOpen(false)
    }

    document.addEventListener('click', onBodyClick ,true)

    return () => document.removeEventListener('click', onBodyClick, true)
  }, [])

  return (
    <div className='ui form' ref={divRef}>
      <div className='field'>
        <label className='label'>{label}</label>
        <div className={`ui selection dropdown ${open ? 'visible active' : ''}`} onClick={() => setOpen(!open)}>
          <i className='dropdown icon' />
          <div className='text'>{selected.label}</div>
          <div className={`menu ${open ? 'visible transition' : ''}`}>
            {renderedOptions}
          </div>
        </div>
      </div>
    </div>
  )
}

Dropdown.propTypes = {
  options: PropTypes.array.isRequired,
  selected: PropTypes.object.isRequired,
  setSelected: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
}

export default Dropdown
