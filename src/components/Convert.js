import React, {useEffect, useState} from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'

const translateAPIKey = 'AIzaSyCHUCmpR7cT_yDFHC98CZJy2LTms-IwDlM'

const Convert = ({language, text}) => {
  const [translated, setTranslated] = useState('')
  const [debouncedText, setDebouncedText] = useState(translated)

  useEffect(() => {
    const doTranslation = async () => {
      const {data} = await axios.post('https://translation.googleapis.com/language/translate/v2', {}, {
        params: {
          q: debouncedText,
          target: language.value,
          key: translateAPIKey
        }
      })

      setTranslated(data.data.translations[0].translatedText)
    }

    doTranslation()
  }, [language, debouncedText])

  useEffect(() => {
    const timerId = setTimeout(() => {
      setDebouncedText(text)
    }, 500)

    return () => {
      clearTimeout(timerId)
    }
  }, [text])

  return (
    <div>
      <h1 className='ui header'>{translated}</h1>
    </div>
  )
}

Convert.propTypes = {
  language: PropTypes.object.isRequired,
  text: PropTypes.string.isRequired
}

export default Convert
